package com.example.ms_pop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

//import cmsc436.tharri16.googlesheetshelper.CMSC436Sheet;
import edu.umd.cmsc436.sheets.Sheets;

import static java.lang.Math.random;

/**
 * Created by lisam on 3/1/2017.
 */

public class PoppingGame extends AppCompatActivity implements Sheets.Host, View.OnClickListener {

    private int numTrials = 0;
    private int numPops = 0;
    Button start_btn;
    Button pop_btn;
    private TextView Lsymbol;
    private TextView Rsymbol;
    private TextView pop_title;
    private TextView data_sent;
    private float responseAvg = 0;
    private float leftHandAvg = 0;
    private float rightHandAvg = 0;
    private int countdown = 5;
    int popCount = 0;
    File myFile;
    OutputStream outputStream;
    boolean isLeftHand = true;
    boolean hasLeftBeenCalled = false;
    boolean hasRightBeenCalled = false;

    // get the supported ids for GMT-08:00 (Pacific Standard Time)
    String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
    // create a Pacific Standard Time time zone
    Calendar calendar = new GregorianCalendar();

    float startSeconds = 0;
    float startMilliseconds = 0;
    float popSeconds = 0;
    float popMilliseconds = 0;
    float secondsDifference = 0;
    float startTime = 0;
    float popTime = 0;

    private static final int LIB_ACCOUNT_NAME_REQUEST_CODE = 1001;
    private static final int LIB_AUTHORIZATION_REQUEST_CODE = 1002;
    private static final int LIB_PERMISSION_REQUEST_CODE = 1003;
    private static final int LIB_PLAY_SERVICES_REQUEST_CODE = 1004;

    private Sheets sheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popping_game);
        isLeftHand = getIntent().getExtras().getBoolean("leftHand");
        numTrials = getIntent().getExtras().getInt("amountTrials");
        numPops = getIntent().getExtras().getInt("amountPops");
        start_btn = (Button) findViewById(R.id.start_button);
        start_btn.setOnClickListener(this);
        pop_btn = (Button) findViewById(R.id.pop_btn);
        pop_btn.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);
        pop_btn.setOnClickListener(this);
        pop_title = (TextView) findViewById(R.id.pop_title);
        data_sent = (TextView) findViewById(R.id.data_sent);
        data_sent.setVisibility(View.INVISIBLE);
        Lsymbol = (TextView) findViewById(R.id.textViewL);
        Rsymbol = (TextView) findViewById(R.id.textViewR);

        if (isLeftHand) {
            Lsymbol.setVisibility(View.VISIBLE);
            Rsymbol.setVisibility(View.INVISIBLE);
        } else {
            Rsymbol.setVisibility(View.VISIBLE);
            Lsymbol.setVisibility(View.INVISIBLE);
        }

        continueButtonClick(isLeftHand);
    }

    private void continueButtonClick(boolean leftHand) {

        isLeftHand = leftHand;
        Runnable task = new Runnable() {
            @Override
            public void run() {
                if (countdown != 0) {
                    start_btn.setVisibility(View.VISIBLE);
                    start_btn.setText("Beginning in " + String.valueOf(countdown) + " seconds");
                    countdown -= 1;
                    final Handler h = new Handler();
                    h.postDelayed(this, 1000);
                } else {
                    start_btn.setEnabled(false);
                    start_btn.setVisibility(View.INVISIBLE);
                    pop_title.setVisibility(View.INVISIBLE);
                    pop_btn.setEnabled(true);
                    popCount = 0;
                    popButtonClick(isLeftHand);
                }
            }
        };

        task.run();
    }

    private void popButtonClick(boolean leftHand) {
        // set up timer
        // create a Pacific Standard Time time zone
        SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);

        // set up rules for Daylight Saving Time
        pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
        pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

        // create a GregorianCalendar with the Pacific Daylight time zone
        // and the current date and time
        calendar = new GregorianCalendar(pdt);
        Date trialTime = new Date();
        calendar.setTime(trialTime);

        isLeftHand = leftHand;

        Runnable task = new Runnable() {
            @Override
            public void run() {

                if (popCount == 0) {
                    final Handler h = new Handler();
                    h.postDelayed(this, 1000);
                    pop_btn.setVisibility(View.INVISIBLE);
                    long delay = (long)Math.random()*3000;

                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            long xMax = (long)(3.25*pop_btn.getMinWidth());
                            int xVal = (int) Math.floor(Math.random() * xMax);
                            long yMax = (long)(9*pop_btn.getMinHeight());
                            int yVal = (int) Math.floor(Math.random() * yMax);
                            pop_btn.setX(xVal);
                            pop_btn.setY(yVal);

                            startSeconds = calendar.get(Calendar.SECOND);
                            startMilliseconds = calendar.get(Calendar.MILLISECOND);
                            startTime = startSeconds + (float)startMilliseconds/1000;
                        }
                    }, delay);

                    popCount++;

                } else if (popCount < numPops+1){
                    pop_btn.setVisibility(View.INVISIBLE);
                    popSeconds = calendar.get(Calendar.SECOND);
                    popMilliseconds = calendar.get(Calendar.MILLISECOND);
                    popTime = popSeconds + (float)popMilliseconds/1000;
                    secondsDifference = popTime - startTime;
                    if (secondsDifference < 0) {
                        secondsDifference = (60 - startTime) + popTime;
                    }

                    System.out.println("Time difference = " + secondsDifference);
                    responseAvg += secondsDifference;
                    //pop_title.setText(secondsDifference + " secs");
                    //pop_title.setVisibility(View.VISIBLE);
                    long delay = 1000 + (long)Math.random()*3000;

                    final Handler h = new Handler();
                    // generate random number between 1-2 seconds and wait that much longer
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            long xMax = (long)(3.25*pop_btn.getMinWidth());
                            int xVal = (int) Math.floor(Math.random() * xMax);
                            long yMax = (long)(9*pop_btn.getMinHeight());
                            int yVal = (int) Math.floor(Math.random() * yMax);
                            pop_btn.setX(xVal);
                            pop_btn.setY(yVal);
                            pop_btn.setVisibility(View.VISIBLE);
                            startSeconds = calendar.get(Calendar.SECOND);
                            startMilliseconds = calendar.get(Calendar.MILLISECOND);
                            startTime = startSeconds + (float)startMilliseconds/1000;
                        }
                    }, delay);

                    popCount++;
                } else { // done with trials
                    pop_btn.setVisibility(View.INVISIBLE);
                    displayAlert(responseAvg/numPops);

                    // next trial
                    if (isLeftHand == true) {
                        leftHandAvg = responseAvg;
                        Lsymbol.setVisibility(View.INVISIBLE);
                        Rsymbol.setVisibility(View.VISIBLE);
                        hasLeftBeenCalled = true;
                        resetValues();
                        if (!hasRightBeenCalled) {
                            continueButtonClick(false);
                        }
                    } else if (isLeftHand == false) {
                        rightHandAvg = responseAvg;
                        Rsymbol.setVisibility(View.VISIBLE);
                        Lsymbol.setVisibility(View.INVISIBLE);
                        hasRightBeenCalled = true;
                        resetValues();
                        if (!hasLeftBeenCalled) {
                            continueButtonClick(true);
                        }
                    }

                    if (hasLeftBeenCalled && hasRightBeenCalled){ // have also already done right, save data
                        createFile();
                        save();
                        sendToSheets(leftHandAvg, rightHandAvg, numPops);
                        data_sent.setVisibility(View.VISIBLE);
                    }
                }
            }
        };

        task.run();
    }

    private void sendToSheets(double leftAvg, double rightAvg, double pops) {
        String userId = "t99p99";

        Sheets sheet = new Sheets(this, getString(R.string.app_name), getString(R.string.CMSC436Sheet_spreadsheet_id_test_sheet));
        sheet.writeData(Sheets.TestType.LH_POP, userId, (float)leftAvg);
        sheet.writeData(Sheets.TestType.RH_POP, userId, (float)rightAvg);
    }

    @Override
    public int getRequestCode(Sheets.Action action) {
        switch (action) {
            case REQUEST_ACCOUNT_NAME:
                return LIB_ACCOUNT_NAME_REQUEST_CODE;
            case REQUEST_AUTHORIZATION:
                return LIB_AUTHORIZATION_REQUEST_CODE;
            case REQUEST_PERMISSIONS:
                return LIB_PERMISSION_REQUEST_CODE;
            case REQUEST_PLAY_SERVICES:
                return LIB_PLAY_SERVICES_REQUEST_CODE;
            default:
                return -1; // boo java doesn't know we exhausted the enum
        }
    }

    @Override
    public void notifyFinished(Exception e) {
        if (e != null) {
            throw new RuntimeException(e); // just to see the exception easily in logcat
        }
        displayAlert((float)3.14);
        Log.i(getClass().getSimpleName(), "Done");
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        this.sheet.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.sheet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pop_btn:
                if (isLeftHand)
                    popButtonClick(true);
                else
                    popButtonClick(false);
                break;

        }
    }

    private void createFile() {
        String FILENAME = "Pop_results";
        try {
            outputStream = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            Log.d("file", "opened");
        } catch (Exception e) {
            File file = new File(FILENAME);
            try {
                file.createNewFile();
                Log.d("file", "created");
                outputStream = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    private void save() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd HH:mm");
        String formattedDate = df.format(c.getTime());
        // patient id, date/time, day, no. hits, avg response time
        String res = formattedDate + "-" + String.valueOf(numPops) + "-" + String.valueOf(responseAvg) + "\n";

        try {
            outputStream.write(res.getBytes());
            outputStream.flush();
            Log.d("write", "output to file");
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void resetValues() {
        responseAvg = 0;
        popCount = 0;
        startSeconds = 0;
        startMilliseconds = 0;
        popSeconds = 0;
        popMilliseconds = 0;
        countdown = 5;
        secondsDifference = 0;
        startTime = 0;
        popTime = 0;
    }

    private void displayAlert(float average){
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Response time average: " + String.valueOf(average)+"\n");
        builder1.setCancelable(true);
        builder1.show();
    }

}

