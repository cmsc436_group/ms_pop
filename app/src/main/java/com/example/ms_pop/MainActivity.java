package com.example.ms_pop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button left_button;
    Button right_button;
    public static int amountPops = 10;
    public static int amountTrials = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        left_button = (Button) findViewById(R.id.left_hand);
        left_button.setOnClickListener(this);
        right_button = (Button) findViewById(R.id.right_hand);
        right_button.setOnClickListener(this);
    }

    private void continueButtonClick(boolean left) {
        Intent intent = new Intent("ms_pop.PoppingGame");
        intent.putExtra("leftHand", left);
        intent.putExtra("amountPops", amountPops);
        intent.putExtra("amountTrials", amountTrials);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.left_hand:
                continueButtonClick(true);
                break;
            case R.id.right_hand:
                continueButtonClick(false);
                break;

        }
    }

}
